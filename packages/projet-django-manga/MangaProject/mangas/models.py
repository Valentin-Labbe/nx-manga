from django.db import models
from django.conf import settings
from django.contrib.auth.models import User, AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.

class Auteur(models.Model):
    name = models.CharField(max_length=100)
    bio = models.CharField(max_length=255)

    def __str__(self):
        return self.name

class Manga(models.Model):
    title = models.CharField(max_length=100)
    release_date = models.DateField()
    img = models.CharField(max_length=255)
    synopsis = models.TextField()
    author = models.ForeignKey(Auteur, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.title

class Utilisateur(AbstractUser):
    username = models.CharField(max_length=30, unique="True", blank=False)
    email = models.EmailField(verbose_name="Email", max_length=200, unique=True)
    wishlist = models.ManyToManyField(Manga, related_name='wish')

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    def __str__(self):
        return self.username


class Aimer(models.Model):
   user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True)
   manga = models.ForeignKey(Manga, on_delete=models.CASCADE, null=True) #related_name
   rate = models.IntegerField(default=0) #, validators=[MaxValueValidator(5), MinValueValidator(0)]
   def __str__(self):
    return self.rate