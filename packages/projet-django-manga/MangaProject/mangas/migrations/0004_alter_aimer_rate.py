# Generated by Django 4.1.1 on 2022-11-25 19:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mangas', '0003_alter_aimer_rate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='aimer',
            name='rate',
            field=models.IntegerField(default=0),
        ),
    ]
