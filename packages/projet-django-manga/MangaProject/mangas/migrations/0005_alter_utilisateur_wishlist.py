# Generated by Django 4.1.1 on 2022-12-01 21:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mangas', '0004_alter_aimer_rate'),
    ]

    operations = [
        migrations.AlterField(
            model_name='utilisateur',
            name='wishlist',
            field=models.ManyToManyField(related_name='wish', to='mangas.manga'),
        ),
    ]
