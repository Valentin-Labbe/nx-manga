from django.urls import path
from django.contrib.auth.views import LoginView, PasswordResetView, PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from . import views, forms

urlpatterns = [
    path("", views.home, name="home"),
    path("login", LoginView.as_view(template_name="authentification/login.html"), name="login"),
    path("logout", views.user_logout, name="logout"),
    path("signup", views.signup, name="signup"),
    path("user_update", views.user_update, name="user_update"),
    path("reset_password", PasswordResetView.as_view(template_name="authentification/password_reset.html", html_email_template_name="authentification/password_reset_mail.html"), {'post_reset_redirect': 'password_reset_done.html'}, name="password_reset"),
    path("reset_password/done", PasswordResetDoneView.as_view(template_name="authentification/password_reset_done.html"), name="password_reset_done"),
    path("reset_password/confirm/<uidb64>/<token>", PasswordResetConfirmView.as_view(template_name="authentification/password_reset_confirm.html"), name="password_reset_confirm"),
    path("reset_password/complete", PasswordResetCompleteView.as_view(template_name="authentification/password_reset_complete.html"), name="password_reset_complete"),
    path("password_change", views.password_change, name="password_change"),
    path("mangas_listing", views.mangas_listing, name="mangas_listing"),
    path("manga_search", views.manga_search, name="manga_search"),
    path("manga_detail/<int:id>", views.manga_detail, name="manga_detail"),
    path("mangas_add", views.mangas_add, name="mangas_add"),
    path("<int:id>/mangas_update", views.mangas_update, name="mangas_update"),
    path("<int:id>/mangas_delete", views.mangas_delete, name="mangas_delete"),
    path("authors_listing", views.authors_listing, name="authors_listing"),
    path("author_searchs", views.authors_search, name="authors_search"),
    path("authors_add", views.authors_add, name="authors_add"),
    path("<int:id>/authors_update", views.authors_update, name="authors_update"),
    path("<int:id>/authors_delete", views.authors_delete, name="authors_delete"),

    path("<int:user>/aimer_mangas_user_listing", views.aimer_mangas_user_listing, name="aimer_mangas_user_listing"),
    
    path("<int:id>/aimer_mangas_user_add", views.aimer_mangas_user_add, name="aimer_mangas_user_add"),
    path('rate/<int:manga_id>/<int:user_id>/<int:rate>', views.rate, name="rate"),
    path("<int:id>/aimer_mangas_user_delete", views.aimer_mangas_user_delete, name="aimer_mangas_user_delete"),
    path("<int:id>/aimer_mangas_user_delete2", views.aimer_mangas_user_delete2, name="aimer_mangas_user_delete2"),
    path("<int:id>/aimer_mangas_user_update", views.aimer_mangas_user_update, name="aimer_mangas_user_update"),


    path("<int:user>/wishlist_listing", views.wishlist_listing, name="wishlist_listing"),

    path("<int:manga>/<int:user>/wishlist_add", views.wishlist_add, name="wishlist_add"),
    path("<int:manga>/<int:user>/wishlist_delete", views.wishlist_delete, name="wishlist_delete"),
    
    path("<int:manga>/<int:user>/wishlist_delete_list_mangas", views.wishlist_delete_list_mangas, name="wishlist_delete_list_mangas"),
]