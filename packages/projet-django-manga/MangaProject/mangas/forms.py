import datetime

from django import forms
from django.forms import ModelForm, Textarea
from django.contrib.auth.forms import UserCreationForm
from mangas.models import Utilisateur, Manga, Auteur, Aimer

class LoginForm(ModelForm):
    class Meta:
        model = Utilisateur
        fields = ("username", "password")

class SignUpForm(UserCreationForm):
    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = "Nom d'utilisateur"
    class Meta:
        model = Utilisateur
        fields = ("username", "email", "password1", "password2")

class UpdateUserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(UpdateUserForm, self).__init__(*args, **kwargs)
        self.fields['username'].label = "Nom d'utilisateur"
    class Meta:
        model = Utilisateur
        fields = ["username", "email"]

class MangaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(MangaForm, self).__init__(*args, **kwargs)
        self.fields['title'].label = "Title"
        self.fields['release_date'].label = "Release_date"
        self.fields['img'].label = "Img URL"
        self.fields['synopsis'].label = "Synopsis"
        self.fields["author"].label="Author"
        
    class Meta:
        model = Manga
        fields = ['title', 'release_date', 'img', 'synopsis', 'author']
        
        widgets = {
            "release_date": forms.DateInput(attrs={"type": "date", 'value': datetime.date.today().strftime("%Y-%m-%d")}, format=("%Y-%m-%d"))
        }

class AuthorForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super(AuthorForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Name"
        self.fields['bio'].label = "Bio"
        
    class Meta:
        model = Auteur
        fields = ['name', 'bio']