from django.shortcuts import render, redirect
from .forms import LoginForm, SignUpForm, MangaForm, AuthorForm, UpdateUserForm
from mangas.models import Manga, Auteur, Aimer, Utilisateur
from django.contrib.auth import login, logout, update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.core.paginator import Paginator

def home(request):
    return render(request, template_name="home.html")

# Authentication views

def user_logout(request):
    logout(request)
    return redirect("home")

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = form.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            return redirect('home')
    else:
        form = SignUpForm()
    return render(request, 'authentification/signup.html', {'form': form})

# Profile view

def user_update(request):
    user = Utilisateur.objects.get(id=request.user.id)
    if request.method == 'POST':
        form = UpdateUserForm(request.POST, instance=user)
        if form.is_valid():
            form.save()
            return redirect('home')
    else:
        form = UpdateUserForm(instance=user)
    return render(request, 'user/user_update.html', {'form': form})

def password_change(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('home')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'user/password_change.html', {'form': form})

# CRUD MANGA

def mangas_listing(request):
    mangas = Manga.objects.all().order_by("title")
    
    paginator = Paginator(mangas, 25)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, template_name="mangas/mangas_listing.html", context={"mangas":page_obj})

def manga_search(request):
    query = request.GET.get("query")
    mangas = Manga.objects.filter(title__icontains=query).order_by("title")

    return render(request, template_name="mangas/mangas_listing.html", context={"mangas": mangas})


def manga_detail(request, id):
    manga = Manga.objects.get(id=id)

    if request.user.is_authenticated:
        liked = is_liked(manga, request.user)
        wished = is_wished(manga, request.user)
        return render(request, "mangas/manga_detail.html", {"manga": manga, "liked": liked, "wished": wished})
    else:
        return render(request, "mangas/manga_detail.html", {"manga": manga})

def mangas_add(request):
    if request.method == "POST":
        form = MangaForm(request.POST).save()
        return redirect('mangas_listing')
    else:
        form=MangaForm()    

    return render(request,'mangas/mangas_add.html',{'form':form})


def mangas_update(request, id):
    manga= Manga.objects.get(id=id)
    form = MangaForm(instance=manga)
    if request.method=="POST":
        form=MangaForm(request.POST, instance=manga)
        if form.is_valid():
            form.save()
            return redirect("mangas_listing")
        else:
            form = MangaForm(instance=manga)

    return render(request, "mangas/mangas_update.html", {"form":form})

def mangas_delete(request, id):
    manga = Manga.objects.get(id=id)  
    if request.method == 'POST':
        manga.delete()
        return redirect('mangas_listing')
    return render(request,'mangas/mangas_delete.html',{'manga': manga})


# CRUD AUTEUR

def authors_listing(request):
    objects = Auteur.objects.all().order_by("name")
    return render(request, template_name="authors/authors_listing.html", context={'authors':objects})

def authors_search(request):
    query = request.GET.get("query")
    objects = Auteur.objects.filter(name__icontains=query).order_by("name")
    return render(request, template_name="authors/authors_listing.html", context={'authors':objects})

def authors_add(request):
    if request.method == "POST":
        form = AuthorForm(request.POST).save()
        return redirect('authors_listing')
    else:
        form=AuthorForm()    

    return render(request,'authors/authors_add.html',{'form':form})


def authors_update(request, id):
    author= Auteur.objects.get(id=id)
    form = AuthorForm(instance=author)
    if request.method=="POST":
        form=AuthorForm(request.POST, instance=author)
        if form.is_valid():
            form.save()
            return redirect("authors_listing")
        else:
            form = AuthorForm(instance=author)

    return render(request, "authors/authors_update.html", {"form":form})

def authors_delete(request, id):
    author = Auteur.objects.get(id=id)  
    if request.method == 'POST':
        author.delete()
        return redirect('authors_listing')
    return render(request,'authors/authors_delete.html',{'author': author})

def aimer_mangas_user_listing(request, user):
    mangas = Aimer.objects.filter(user=user)
    user = Utilisateur.objects.get(id=user)

    paginator = Paginator(mangas, 25)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    return render(request, template_name="aimer/aimer_mangas_user_listing.html", context={'mangas':page_obj, "user":user})


def aimer_mangas_user_add(request, id):
    manga=Manga.objects.get(id=id)
    rating, _ = Aimer.objects.get_or_create(manga=manga, user=request.user)

    return render(request,'aimer/aimer_mangas_user_add.html', {"manga":manga, "rate":rating.rate})

def aimer_mangas_user_delete(request, id):
    aimer = Aimer.objects.get(id=id)  
    aimer.delete()
    return redirect('aimer_mangas_user_listing', user=request.user.id)

def aimer_mangas_user_delete2(request, id):
    aimer = Aimer.objects.get(user=request.user, manga=id)  
    aimer.delete()
    return redirect('manga_detail', id=id)

def aimer_mangas_user_update(request, id):
    aimer= Aimer.objects.get(id=id)
    form = AimerForm2(instance=aimer)
    if request.method=="POST":
        form=AimerForm2(request.POST, instance=aimer)
        if form.is_valid():
            form.save()
            return redirect("mangas_listing")
        else:
            form = AimerForm2(instance=aimer)

    return render(request, "aimer/aimer_mangas_user_update.html", {"form":form, "aimer":aimer})

def wishlist_add(request, manga, user):
    manga = Manga.objects.get(id=manga)
    user = Utilisateur.objects.get(id=user)
    user.wishlist.add(manga)
    return redirect('manga_detail', id=manga.id)
    
def wishlist_listing(request, user):
    user=Utilisateur.objects.get(id=user)

    paginator = Paginator(user.wishlist.all(), 25)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)


    return render(request, "wishlist/wishlist_listing.html", {"mangas":page_obj, "user":user})


def wishlist_delete(request,manga, user):
    manga = Manga.objects.get(id=manga)
    user = Utilisateur.objects.get(id=user)
    user.wishlist.remove(manga)
    return redirect('wishlist_listing' , user=user.id)

def wishlist_delete_list_mangas(request, manga, user):
    manga = Manga.objects.get(id=manga)
    user = Utilisateur.objects.get(id=user)
    user.wishlist.remove(manga)
    return redirect('manga_detail', id=manga.id)

def is_liked(manga, user):
    try:
        liked = Aimer.objects.get(manga=manga, user=user)
    except Aimer.DoesNotExist:
        liked = None
    return liked is not None

def is_wished(manga, user):
    wished = Manga.objects.filter(wish=user.id, title=manga)
    return len(wished) != 0

def rate(request, manga_id, user_id, rate):
    manga = Manga.objects.get(id=manga_id)
    user = Utilisateur.objects.get(id=user_id)
    rating = Aimer.objects.get(manga=manga, user=user)
    rating.rate = rate
    rating.save()
    return aimer_mangas_user_add(request, manga.id)

def id_is_liked(manga,user):
    try:
        liked = Aimer.objects.get(manga=manga, user=user)
        return liked.id
    except Aimer.DoesNotExist:
        liked = None
        return -1