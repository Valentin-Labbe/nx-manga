import json
from django.core.management.base import BaseCommand
from mangas.models import Auteur, Manga


class Command(BaseCommand):
    help = 'Populate database from given json file'

    def add_arguments(self, parser):
        parser.add_argument('--file', type=str)

    def handle(self, *args, **options):
        with open(options['file'], 'r') as mangasList:
            mangas = json.load(mangasList)
            for manga in mangas:
                newManga,_ = Manga.objects.get_or_create(title=manga["title"], release_date=manga["release_date"], img=manga["img"], synopsis=manga["synopsis"])
                newManga.author, _ = Auteur.objects.get_or_create(name=manga["author"]["name"], bio=manga["author"]["bio"])
                # newManga, _ = PricingPlan.objects.get_or_create(name=plan['name'])
                # newManga.amount = plan['amount']
                newManga.save()

        self.stdout.write(self.style.SUCCESS('Successfully created mangas'))