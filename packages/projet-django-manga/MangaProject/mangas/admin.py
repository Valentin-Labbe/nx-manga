from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from mangas.models import Utilisateur, Manga, Aimer, Auteur

class MangaAdmin(admin.ModelAdmin):
    list_display = ("title", "author", "synopsis", "img", "release_date")

class AuteurAdmin(admin.ModelAdmin):
    list_display = ("name", "bio")

class AimerAdmin(admin.ModelAdmin):
    list_display = ("user", "manga")

admin.site.register(Manga, MangaAdmin)
admin.site.register(Aimer, AimerAdmin)
admin.site.register(Auteur, AuteurAdmin)
admin.site.register(Utilisateur, UserAdmin)
