import requests
import json
import sys
import time
import os
from json import JSONEncoder

class Auteur:
    def __init__(self, name, bio):
        self.name = name
        self.bio = bio

    def __str__(self):
        return self.name

class Manga:
    def __init__(self, title, img, author, release_date, synopsis):
        self.title = title
        self.img = img
        self.author = author
        self.release_date = release_date
        self.synopsis = synopsis

    def __str__(self):
        return self.title

class MangaEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__

def fetch(manga_id):
    r = requests.get("https://api.jikan.moe/v4/manga/"+str(manga_id)+"/full")
    if r.status_code == 200:
        data = json.loads(r.text)

        if len(data) > 1: 
            # Insensé mais l'API renvoie parfois un status code 200 pour une ressource 404. data est alors interpréré par le JSON loader sous la forme :
            # {'status': 404, 'type': 'BadResponseException', 'message': 'Resource does not exist', 'error': '404 on https://myanimelist.net/manga/6/'}
            # d'où la présence de cette conditionnelle
            return None

        manga = data["data"]
        author = manga["authors"][0]

        if (author["name"] and author["url"] and manga["title"] and manga["images"]["jpg"]["image_url"] and manga["published"]["from"] and manga["synopsis"]) != None:
            author_object = Auteur(author["name"].replace(",", ""), author["url"])
            manga_object = Manga(manga["title"], manga["images"]["jpg"]["image_url"], author_object, manga["published"]["from"][0:10], manga["synopsis"])
            manga_to_json = json.dumps(manga_object, cls=MangaEncoder)
            return json.loads(manga_to_json)
        return None
    return None

def progressbar(status, end, manga_title):
    progress = 100 * (status / float(end))
    bar = "█" * int(progress) + "-" * (100 - int(progress))
    print(f"\r\x1b[0;33;40m|{bar}|\x1b[0m \x1b[1;36;40m{progress:.2f}%\x1b[0m \t\x1b[0;33;40mAjout de {manga_title}\x1b[0m", end="\r")
    print(end="\x1b[2K") # Efface la ligne de texte (pour éviter que les print se superposent)
    if status == end:
        print(f"\r\x1b[1;32;40m|{bar}|\x1b[0m \x1b[1;36;40m{progress:.2f}%\x1b[0m", end="\r")

def create_json_file(manga_amount, filename):
    delay = 1 if manga_amount > 60 else 0.34 # Définition d'un délais pour satisfaire le nombre limité de requêtes tolérées par l'API. (3 par secondes et 60 par minutes au maximum)
    manga_list = []
    found = 0
    i = 1

    print("\n")
    while found < manga_amount:
        fetched_manga = fetch(i)
        if fetched_manga is not None:
            manga_list.append(fetched_manga)
            # print(str(i) + ". Récupéré")
            found += 1
            progressbar(found, manga_amount, fetched_manga["title"])
        #else:
            # print(str(i) + ". Non trouvé")
        i += 1
        time.sleep(delay) # L'API limite le nombre de requête à 60 par minute. D'où la présence d'une pause pour ne pas "sauter" un manga
    print("\n")

    with open(filename, "w") as outputfile:
        outputfile.write(json.dumps(manga_list, indent=4))
    print(str(found) + " mangas ajoutés dans " + filename)

def main(args):
    usage_msg = "\nVeuillez utiliser : python fetching.py <nombreDeMangas> <nomDuFichier>\n"

    if len(args) < 3:
        print("ERREUR : Arguments manquants." + usage_msg)
    elif len(args) > 3:
        print("ERREUR : Trop d'arguments. Seuls 2 sont attendus." + usage_msg)
    elif len(args[1]) < 1:
        print("ERREUR : Veuillez préciser une quantité de mangas supérieure à 1")
    elif len(args[2]) < 6:
        print("ERREUR : Fichier incorrect")
    elif (args[2][len(args[2]) - 5 : len(args[2])]) != ".json":
        print("ERREUR : Veuillez utiliser un fichier JSON")
    else:
        create_json_file(int(args[1]), args[2])

os.system('mode con: cols=200 lines=40')
main(sys.argv)